﻿using UnityEngine;
using System.Collections;

public class ModelController : MonoBehaviour {
    private float attribute1;
    private Renderer render;
	// Use this for initialization
	void Start () {
        render = GetComponentInChildren<Renderer>();
	}
	
	// Update is called once per frame
	public void SetAttr (float attr) {
        attribute1 = attr;
        render.sharedMaterial.SetColor("_Color", Color.blue * attribute1);
	}
}