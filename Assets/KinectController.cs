﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Kinect;
using Windows.Kinect;
using Microsoft.Kinect.VisualGestureBuilder;




public class KinectController : MonoBehaviour
{
    public Texture2D redHand;
    public Texture2D greenHand;
    public Texture2D greyHand;
    public Transform camModelMode;
    public Transform camCarouselMode;

    public static bool debug;

    public static int MaxComponent(Vector3 v)
    {
        float max = Mathf.Abs(v[0]);
        int mi = 0;
        for (int i = 1; i < 3; ++i)
        {
            if (Mathf.Abs(v[i]) > max)
            {
                max = Mathf.Abs(v[i]);
                mi = i;
            }
        }
        return mi;
    }
    public static void KeepComponents(Vector3 v, int[] c)
    {
        for (int i = 0; i < 3; ++i)
        {
            v[i] = (c.Contains(i)) ? v[i] : 0;
        }
    }
    public static Vector3 ToVector3(CameraSpacePoint position)
    {
        return new Vector3(position.X, position.Y, position.Z);
    }

    class Arm
    {
        public Vector3 position;
        public HandState handState;
        public TrackingConfidence handStateConfidence;
        public TrackingState trackingState;
        public Vector3 direction;
        public Vector3 averageDirection;
        public Vector3 sumDirection;
        public HandState state;
        public float closedConfidence = 0.0f;

        private Vector3 debugOrigin = new Vector3(0, 0, -14);
        private float debugDuration = 30f;
        public Arm() { }
        public Arm(Vector3 position, HandState handState, TrackingConfidence handStateConfidence, TrackingState trackingState)
        {
            this.position = position; this.handState = handState; this.handStateConfidence = handStateConfidence; this.trackingState = trackingState;
        }

        public void Calculate(Arm from)
        {
            state = from.state;
            averageDirection = from.averageDirection;
            direction = position - from.position;
            closedConfidence = from.closedConfidence;
            sumDirection = from.sumDirection;
            bool newMovement = false;

            float angleDifference = Vector3.Angle(direction, averageDirection);
            if (angleDifference > 40f)
            {
                state = HandState.Unknown;
                closedConfidence = 0.0f;
                newMovement = true;
                //Debug.DrawLine(debugOrigin - from.position, debugOrigin - from.position - direction, Color.black, debugDuration);
            }

            if (state != handState)
            {
                if (handState == HandState.Closed || handState == HandState.Open)
                {
                    state = handState;
                    newMovement = true;
                    //Debug.DrawLine(debugOrigin - from.position, debugOrigin - from.position - Vector3.up * 0.01f, Color.yellow, debugDuration);
                }
            }

            if (handState == HandState.Closed)
            {
                closedConfidence = Mathf.Clamp01(closedConfidence + 0.32f * Mathf.Clamp01((float)handStateConfidence + 0.5f));
                if (closedConfidence < 0.7f)
                    state = HandState.Unknown;
            }
            else if (handState == HandState.Open)
            {
                closedConfidence = Mathf.Clamp01(closedConfidence - 0.32f);
                if (state == HandState.Closed && closedConfidence < 0.9f)
                    state = HandState.Unknown;
            }

            if (newMovement)
            {
                averageDirection = direction;
                sumDirection = direction;
            }
            else
            {
                sumDirection += direction;
                averageDirection = averageDirection * 0.5f + direction * 0.5f;
            }

            if (debug)
            {
                if (handState == HandState.Open)
                    Debug.DrawLine(debugOrigin - from.position, debugOrigin - from.position + Vector3.up * 0.01f, Color.green, debugDuration);
                if (handState == HandState.Closed)
                    Debug.DrawLine(debugOrigin - from.position, debugOrigin - from.position + Vector3.up * 0.01f, Color.red * Mathf.Clamp01((float)handStateConfidence + 0.5f), debugDuration);

                if (state == HandState.Closed)
                {
                    Debug.DrawLine(debugOrigin - from.position, debugOrigin - from.position - direction, Color.red * closedConfidence, debugDuration);
                    Debug.DrawLine(debugOrigin - from.position, debugOrigin - from.position - averageDirection * 0.08f, Color.green, debugDuration);
                }
                else if (state == HandState.Open)
                    Debug.DrawLine(debugOrigin - from.position, debugOrigin - from.position - direction, Color.green, debugDuration);
            }
        }

        public enum Gesture {
            None,
            Move,
            Open,
            Close,
            Slider
        }
        public static Gesture Recognize(Arm left, Arm right)
        {
            // Slider gesture
            if (left.state == HandState.Open &&
                right.state == HandState.Closed &&
                left.direction.magnitude < 0.2f &&
                (left.position - right.position).y < 0.4f)
            {
                return Gesture.Slider;
            } else if (right.state == HandState.Closed)
            {
                return Gesture.Move;
            } else if (left.state == HandState.Open &&
                right.state == HandState.Open &&
                MaxComponent(left.direction) == 0 &&
                MaxComponent(right.direction) == 0 &&
                left.sumDirection.x < -0.1f &&
                right.sumDirection.x > 0.1f)
                //Mathf.Sign(left.direction.x) == -1 &&
                //Mathf.Sign(right.direction.x) == 1)
            {
                return Gesture.Open;
            } else if (left.state == HandState.Open &&
                right.state == HandState.Open &&
                MaxComponent(left.direction) == 0 &&
                MaxComponent(right.direction) == 0 &&
                left.sumDirection.x > 0.1f &&
                right.sumDirection.x < -0.1f)
                //Mathf.Sign(left.direction.x) == 1 &&
                //Mathf.Sign(right.direction.x) == -1)
            {
                return Gesture.Close;
            }
            
            return Gesture.None;
        }
    }

    class Target
    {
        public GameObject gameObject;
        public Renderer renderer;
        public Rigidbody rigidbody;
        public Collider collider;
        public Vector3 rotationSpeed;
        public Vector3 transformSpeed;

        public Target (GameObject target)
        {
            gameObject = target;
            renderer = gameObject.GetComponentInChildren<Renderer>();
            rigidbody = gameObject.GetComponentInChildren<Rigidbody>();
            collider = gameObject.GetComponentInChildren<Collider>();
        }
    }
    private KinectSensor _Sensor;
    private BodyFrameReader _Reader;
    private Body[] _Data = null;

    private Arm leftArm, rightArm, leftArmLast, rightArmLast;
    private Target target;
    private float drag;
    private float dragLerp;
    private Quaternion dragQuaternion;
    private Vector3 dragPosition;
    private Camera cam;
    private bool newKinectFrame;
    private bool selectingTarget;
    private Quaternion selectObjectRotation;
    private float gestureProgress;
    private float selectProgress;
    private List<GameObject> objects;
    private Arm.Gesture gesture;
    private Arm.Gesture lastGesture;

    // Initialization
    void Start()
    {
        InitKinect();
        cam = Camera.main;
        leftArm = new Arm();
        rightArm = new Arm();
        leftArmLast = new Arm();
        rightArmLast = new Arm();
        drag = 1.0f;
        objects = new List<GameObject>();
        foreach (Transform child in transform)
        {
            objects.Add(child.gameObject);
            //Renderer rend = child.gameObject.GetComponentInChildren<Renderer>();
        }
        target = new Target(objects.First());
        selectingTarget = true;
        selectObjectRotation = transform.rotation;
    }

    void InitKinect()
    {
        _Sensor = KinectSensor.GetDefault();

        if (_Sensor != null)
        {
            _Reader = _Sensor.BodyFrameSource.OpenReader();

            if (!_Sensor.IsOpen)
            {
                _Sensor.Open();
            }
        }
    }

    void DestroyKinect()
    {
        if (_Reader != null)
        {
            _Reader.Dispose();
            _Reader = null;
        }

        if (_Sensor != null)
        {
            if (_Sensor.IsOpen)
            {
                _Sensor.Close();
            }
            _Sensor = null;
        }
    }

    void OnApplicationQuit()
    {
        DestroyKinect();
    }

    // Each frame
    private Vector3 driftCorrection;
    void Update()
    {
        Vector3 inputVector = Vector3.zero;
        ProcessKinect();

        if (newKinectFrame)
        {
            rightArm.Calculate(rightArmLast);
            leftArm.Calculate(leftArmLast);

            lastGesture = gesture;
            gesture = Arm.Recognize(leftArm, rightArm);

            if (gesture != lastGesture) {
                if (lastGesture == Arm.Gesture.Open)
                    selectingTarget = false;
                if (lastGesture == Arm.Gesture.Close)
                    selectingTarget = true;
                //Debug.Log(gesture.ToString() + " " + rightArm.sumDirection + " " + Time.realtimeSinceStartup);
            }

            // Filter
            if (gesture == Arm.Gesture.Move)
            {
                float rightMajorAxis = Mathf.Max(Mathf.Abs(rightArm.averageDirection.x), Mathf.Abs(rightArm.averageDirection.y), Mathf.Abs(rightArm.averageDirection.z));
                if (rightMajorAxis == Mathf.Abs(rightArm.averageDirection.x) || rightMajorAxis == Mathf.Abs(rightArm.averageDirection.y))
                    rightArm.direction.Set(rightArm.direction.x, rightArm.direction.y, 0);
                else if (rightMajorAxis == Mathf.Abs(rightArm.averageDirection.z))
                    rightArm.direction.Set(0, 0, -rightArm.direction.z);
            }
        }
        if (gesture == Arm.Gesture.Slider)
        {
            gestureProgress = Mathf.Clamp01(Mathf.Abs(leftArm.position.x - rightArm.position.x) / 0.6f);
            ModelController model = target.gameObject.GetComponent<ModelController>();
            model.SetAttr(gestureProgress);
        }
        if (gesture == Arm.Gesture.Open) {
            //gestureProgress = Mathf.Clamp01(((leftArm.sumDirection - rightArm.sumDirection).magnitude - 0.1f) / 1.0f);
            gestureProgress = Mathf.Clamp01((Mathf.Abs(leftArm.sumDirection.x - rightArm.sumDirection.x) - 0.3f ) / 0.3f);
            //Debug.Log(gestureProgress.ToString() + " " + leftArm.sumDirection.x + " " + rightArm.sumDirection.x + " " + Time.realtimeSinceStartup);
            if (gestureProgress == 1.0f)
                selectingTarget = true;
            cam.transform.position = Vector3.Lerp(camCarouselMode.position, camModelMode.position, gestureProgress); 
        }
        if (gesture == Arm.Gesture.Close) {
            //gestureProgress = Mathf.Clamp01(((leftArm.sumDirection - rightArm.sumDirection).magnitude - 0.1f) / 1.0f);
            gestureProgress = Mathf.Clamp01((Mathf.Abs(leftArm.sumDirection.x - rightArm.sumDirection.x) - 0.3f) / 0.3f);
            //Debug.Log(gestureProgress.ToString() + " " + leftArm.sumDirection.x + " " + rightArm.sumDirection.x + " " + Time.realtimeSinceStartup);
            if (gestureProgress == 1.0f)
                selectingTarget = false;
            cam.transform.position = Vector3.Lerp(camModelMode.position, camCarouselMode.position, gestureProgress); 
        }
        if (selectingTarget && gesture == Arm.Gesture.Move)
        {
            selectProgress += rightArm.direction.x / 2;
            //Debug.Log(selectProgress.ToString() + " " + rightArm.sumDirection.x + " " + Time.realtimeSinceStartup);
        }
        // We might not have a new frame but lets keep old value
        if (!selectingTarget && gesture == Arm.Gesture.Move)
            inputVector = rightArm.direction;
        
        
        if (Input.GetKey("up"))
            inputVector.y += 0.02f;
        if (Input.GetKey("down"))
            inputVector.y -= 0.02f;
        if (Input.GetKey("left"))
            inputVector.x -= 0.02f;
        if (Input.GetKey("right"))
            inputVector.x += 0.02f;
        if (Input.GetKey(KeyCode.Equals))
            inputVector.z += 0.03f;
        if (Input.GetKey(KeyCode.Minus))
            inputVector.z -= 0.03f;
        if (Input.GetKeyDown(KeyCode.T)) {
            selectingTarget = !selectingTarget;
            cam.transform.position = (selectingTarget) ? camCarouselMode.position : camModelMode.position;
        }
        if (Input.GetKeyDown(KeyCode.D)) {
            debug = !debug;
            GameObject[] views = GameObject.FindGameObjectsWithTag("DebugViews");
            foreach (GameObject view in views)
            {
                view.SetActive(debug);
            }
        }


        // Selecting next model
        if (selectingTarget && (gesture == Arm.Gesture.Move || inputVector != Vector3.zero))
        {
            selectProgress = Mathf.Clamp(selectProgress - inputVector.x * Time.deltaTime * 32, -1.0f, 1.0f);
            int targetIndex = objects.IndexOf(target.gameObject);
            if ((selectProgress > 0.5f &&
                targetIndex < objects.Count - 1) ||
                (selectProgress < -0.5f && targetIndex > 0))
            {
                
                //target.renderer.enabled = false;
                target = new Target(objects.ElementAt(targetIndex + (int)Mathf.Sign(selectProgress) * 1));
                selectObjectRotation = selectObjectRotation * Quaternion.Euler(0, Mathf.Sign(selectProgress) * -90, 0);
                selectProgress = selectProgress - Mathf.Sign(selectProgress) * 1.0f;
            }
            transform.rotation = Quaternion.Lerp(selectObjectRotation, selectObjectRotation * Quaternion.Euler(0, Mathf.Sign(selectProgress) * -90, 0), Mathf.Abs(selectProgress));
        }
        else if (selectingTarget)
        {
            selectProgress = Mathf.Clamp(selectProgress - Mathf.Sign(selectProgress) * Mathf.Min(Time.deltaTime,Mathf.Abs(selectProgress)), -1.0f, 1.0f);
            transform.rotation = Quaternion.Lerp(selectObjectRotation, selectObjectRotation * Quaternion.Euler(0, Mathf.Sign(selectProgress) * -90, 0), Mathf.Abs(selectProgress));
        }

        // Compute translation and rotation speed
        Vector3 TowardsCamera = Camera.main.transform.position - target.renderer.bounds.center;
        TowardsCamera = TowardsCamera / TowardsCamera.magnitude;
        // Check if we are actively moving or just apply momentum
        if (!selectingTarget && inputVector != Vector3.zero)
        {
            target.transformSpeed = TowardsCamera * drag * inputVector.z * 4;
            target.rotationSpeed = new Vector3(inputVector.y, inputVector.x, 0.0f) * drag * 400;
            dragLerp = 0;
        }
        else
        {
            if (drag < 1.0f)
            {
                dragLerp = Mathf.Clamp01(dragLerp + Time.deltaTime * 20);
                driftCorrection = new Vector3(0, 0, dragPosition.z) - target.renderer.bounds.center;
                //driftCorrection = Vector3.zero;
                if (KinectController.debug)
                {
                    Debug.DrawLine(dragPosition, dragPosition + Vector3.up / 2, Color.yellow, 0, false);
                }
                if (Mathf.Sign(driftCorrection.z) != Mathf.Sign(-TowardsCamera.z))
                    driftCorrection.z = -driftCorrection.z;
                target.rigidbody.position = Vector3.Lerp(target.rigidbody.position, dragPosition + driftCorrection, dragLerp);
                //print(dragPosition - driftCorrection);
                target.gameObject.transform.rotation = Quaternion.Lerp(target.gameObject.transform.rotation, dragQuaternion, dragLerp);
                target.transformSpeed = Vector3.zero;
                target.rotationSpeed = Vector3.zero;
            }
            else
            {
                target.transformSpeed = target.transformSpeed / 1.25f;
                target.rotationSpeed = target.rotationSpeed / 1.05f;
            }
        }

        // Apply speeds
        if (debug)
        {
            Debug.DrawLine(target.renderer.bounds.center, target.renderer.bounds.center + Vector3.up / 2, Color.blue, 0, false);
        }
        if (target.rotationSpeed != Vector3.zero || target.transformSpeed != Vector3.zero)
        {
            target.gameObject.transform.RotateAround(target.renderer.bounds.center, Vector3.right, target.rotationSpeed.x * Time.deltaTime * 10);
            target.gameObject.transform.RotateAround(target.renderer.bounds.center, Vector3.up, target.rotationSpeed.y * Time.deltaTime * 10);
            //target.rigidbody.angularVelocity = Vector3.right;
            //target.rigidbody.AddRelativeTorque(target.rotationSpeed * Time.deltaTime * 10);
            //target.rigidbody.AddTorque(Vector3.up);
            //target.gameObject.transform.position += target.transformSpeed * Time.deltaTime * 10;
            target.rigidbody.position += target.transformSpeed * Time.deltaTime * 10;
        } 
        // Clear flags
        newKinectFrame = false;
    }

    void OnGUI()
    {
        if (debug)
        {
            GUI.TextArea(new Rect(10, 35, 400, 25), "Tar " + target.transformSpeed.ToString() + target.rotationSpeed.ToString() + " " + drag.ToString() + " " + dragLerp);
            GUI.TextArea(new Rect(10, 60, 400, 25), "Cur " + target.rigidbody.position.ToString() + " " + target.gameObject.transform.rotation.eulerAngles.ToString() + " " + target.renderer.bounds.center);
            GUI.TextArea(new Rect(10, 85, 400, 25), "Ret " + dragPosition.ToString() + " " + dragQuaternion.eulerAngles.ToString() + " " + Vector3.Lerp(target.rigidbody.position, dragPosition + driftCorrection, dragLerp));
            GUI.TextArea(new Rect(10, 110, 400, 25), "Ges " + gesture + " " + selectingTarget + " " + gestureProgress + " " + selectProgress);
        }

        Texture2D leftHand = greyHand;
        if (leftArm.handState == HandState.Closed)
                leftHand = redHand;
        else if (leftArm.handState == HandState.Open)
                leftHand = greenHand;
        Texture2D rightHand = greyHand;
        if (rightArm.handState == HandState.Closed)
                rightHand = redHand;
        else if (rightArm.handState == HandState.Open)
                rightHand = greenHand;
        
        float handSize = 96;
        GUI.DrawTexture(new Rect(16, Screen.height - 16 - handSize, handSize, handSize), leftHand);
        GUI.DrawTexture(new Rect(Screen.width - 16 - handSize, Screen.height - 16 - handSize, handSize, handSize), rightHand);
    }

    System.TimeSpan lastBodyFrameTime;
    void ProcessKinect()
    {
        if (_Reader != null)
        {
            var frame = _Reader.AcquireLatestFrame();
            
            if (frame != null)
            {
                if (lastBodyFrameTime == frame.RelativeTime)
                    return;
                lastBodyFrameTime = frame.RelativeTime;
                newKinectFrame = true;

                if (_Data == null)
                {
                    _Data = new Body[_Sensor.BodyFrameSource.BodyCount];
                }

                frame.GetAndRefreshBodyData(_Data);

                frame.Dispose();
                frame = null;

                int idx = -1;
                for (int i = 0; i < _Sensor.BodyFrameSource.BodyCount; i++)
                {
                    if (_Data[i].IsTracked)
                    {
                        idx = i;
                        break;
                    }
                }

                if (idx > -1)
                {
                    rightArmLast = rightArm;
                    rightArm = new Arm(
                        ToVector3(_Data[idx].Joints[JointType.ShoulderRight].Position) - ToVector3(_Data[idx].Joints[JointType.HandRight].Position),
                        _Data[idx].HandRightState,
                        _Data[idx].HandRightConfidence,
                        (TrackingState) Mathf.Min(
                            (int)_Data[idx].Joints[JointType.ShoulderRight].TrackingState,
                            (int)_Data[idx].Joints[JointType.ElbowRight].TrackingState,
                            (int)_Data[idx].Joints[JointType.HandRight].TrackingState)
                        );

                    leftArmLast = leftArm;
                    leftArm = new Arm(
                        ToVector3(_Data[idx].Joints[JointType.ShoulderLeft].Position) - ToVector3(_Data[idx].Joints[JointType.HandLeft].Position),
                        _Data[idx].HandLeftState,
                        _Data[idx].HandLeftConfidence,
                        (TrackingState) Mathf.Min(
                            (int)_Data[idx].Joints[JointType.ShoulderLeft].TrackingState,
                            (int)_Data[idx].Joints[JointType.ElbowLeft].TrackingState,
                            (int)_Data[idx].Joints[JointType.HandLeft].TrackingState)
                        );

                }
            }
        }
    }

    public void OnCollisionEnter()
    {
        // Save the position and rotation
        dragPosition = target.rigidbody.position;
        dragQuaternion = target.gameObject.transform.rotation;
    }

    public void OnCollisionExit()
    {
        drag = 1.0f;
    }

    public void OnTouch(float distance, Vector3 point, Vector3 normal)
    {
        drag = Mathf.Clamp((distance - 0.2f) / 0.2f, 0, 1);
    }
}