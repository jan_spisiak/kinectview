﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    KinectController kinectController;
    Vector3 lastLookingAt;
    Vector3 lookingAt;
    float lookingAtLerp;
    float lookingAtLerpTime;

    // Use this for initialization
    void Start()
    {
        kinectController = GameObject.Find("KinectController").GetComponent<KinectController>();
        lookingAt = Vector3.zero;
        lookingAtLerp = 1.0f;
        lookingAtLerpTime = 1.0f;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (lookingAt != lastLookingAt)
        {
            float angle = Quaternion.Angle(Quaternion.LookRotation(lookingAt - transform.position), Quaternion.LookRotation(lastLookingAt - transform.position));
            if (angle > 20.0f)
                lookingAtLerpTime = 20.0f;
            else
                lookingAtLerpTime = Mathf.Max(angle / 2, 1);
            lookingAtLerp = Time.deltaTime;
            lastLookingAt = lookingAt;
            //print(lookingAt.ToString("F4") + Time.realtimeSinceStartup);
        }
        else
        {
            lookingAtLerp += Time.deltaTime;
        }
        if (lookingAtLerp > lookingAtLerpTime)
            lookingAtLerp = lookingAtLerpTime;
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(lookingAt - transform.position), lookingAtLerp / lookingAtLerpTime);
    }

    void OnGUI()
    {
        if (KinectController.debug)
        {
            GUI.TextArea(new Rect(10, 10, 300, 25), lookingAt.ToString() + " " + (lookingAtLerp / lookingAtLerpTime).ToString());
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        kinectController.OnCollisionEnter();
    }

    void OnCollisionStay(Collision collision)
    {
        Vector3 averageContactPoint = new Vector3();
        Vector3 averageContactNormal = new Vector3();
        foreach (ContactPoint contact in collision.contacts)
        {
            averageContactPoint += contact.point;
            averageContactNormal += contact.normal;
            if (KinectController.debug)
            {
                Debug.DrawRay(contact.point, contact.normal, Color.white);
                Debug.DrawLine(contact.point, contact.point + Vector3.up / 10);
            }
        }
        averageContactNormal.Normalize();
        averageContactPoint /= collision.contacts.Length;
        if (KinectController.debug)
        {
            Debug.DrawRay(averageContactPoint, averageContactNormal, Color.green);
            Debug.DrawLine(averageContactPoint, averageContactPoint + Vector3.up / 10, Color.red);
        }
        lookingAt = averageContactPoint;
        //print(collision.gameObject.name);
        float distance =  Vector3.Distance(transform.position, averageContactPoint);
        kinectController.OnTouch(distance, averageContactPoint, averageContactNormal);
        //collision.transform.position -= averageContactNormal / 5;
    }

    void OnCollisionExit(Collision collision)
    {
        lookingAt = Vector3.zero;
        kinectController.OnCollisionExit();
    }
}